import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// ---------- import screen ---------- //
import HomeScreen from '../screens/HomeScreen/index';
import LoginScreen from '../screens/LoginScreen/index';

const RootStack = createSwitchNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null,
    },
  },
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null,
    },
  },
});

export default createAppContainer(RootStack);
