import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationParams,
} from 'react-navigation';
import styles from './styles';
import ChatView from '../../components/chatView/ChatView';
import {onLoadMessage} from '../../services/FireBase/firebase';

const {width, height} = Dimensions.get('screen');
interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface State {
  source: Message[];
  newMessage?: Message;
}

type Message = {
  user: string;
  mes: string;
};

const roomPath = 'room1';
const userID = 'user1';

export default class index extends Component<Props, State, {}> {
  constructor(props: Props) {
    super(props);
    this.state = {
      source: [],
    };
  }

  componentDidMount = async () => {
    let ref = await onLoadMessage({roomPath});
    for (let i = ref.length - 1; i >= 0; i--) {
      this.state.source.push({user: ref[i].ID_user, mes: ref[i].mes});
    }
    this.setState({source: this.state.source});
  };

  _loadMoreOlderMessage = async () => {
    this.setState({
      source: [...[{user: 'user1', mes: 'hello 3'}], ...this.state.source],
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {/* --- button logOut --- */}
        <TouchableOpacity
          style={styles.btnLogOut}
          onPress={() => this.props.navigation.navigate('Login')}>
          <Text> Đăng xuất </Text>
        </TouchableOpacity>
        {/* --- chat space --- */}

        <View style={styles.chatSpace}>
          <ChatView
            width="100%"
            height="100%"
            userID={userID}
            source={this.state.source}
            onListenNewMessage={() => {
              this.state.newMessage &&
                this.state.source.push(this.state.newMessage);
            }}
            onSendMessage={mes => {
              this.setState({
                source: [...this.state.source, {user: 'user1', mes: mes}],
              });
            }}
            onPullToLoadMore={() => this._loadMoreOlderMessage()}
          />
        </View>
      </SafeAreaView>
    );
  }
}
