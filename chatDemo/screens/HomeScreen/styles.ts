import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {flex: 1},
  //   subContainer: {width: '100%', height: '100%'},
  btnLogOut: {},
  chatSpace: {
    flex: 1,
    alignSelf: 'center',
  },
});

export default styles;
