import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
} from 'react-native';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationParams,
} from 'react-navigation';
import styles from './styles';
import {connect} from 'react-redux';
import * as actions from '../../services/redux/checkLogin/Action';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
  counterIncrease?: any;
  counterDecrease?: any;
  counter?: any;
}

class index extends Component<Props> {
  state = {
    userName: 'ABCDEF',
    password: '123456',
    arrRange: [],
    couter: 0,
  };

  handleIncrease = () => {
    this.props.counterIncrease();
  };

  handleDecrease = () => {
    this.props.counterDecrease();
  };

  render() {
    console.log('child: ', this.props.counter);
    return (
      <SafeAreaView style={styles.container}>
        {/* --- form input --- */}
        <View style={styles.formLogin}>
          <View style={styles.formInput}>
            <View style={styles.rowInput}>
              <Text style={styles.txtLeftRowInput}>Tài Khoản</Text>
              <TextInput
                style={styles.txtRightRowInput}
                onChangeText={text => this.setState({userName: text})}
                value={this.state.userName}
              />
            </View>
            <View style={styles.rowInput}>
              <Text style={styles.txtLeftRowInput}>Mật khẩu</Text>
              <TextInput
                style={styles.txtRightRowInput}
                onChangeText={text => this.setState({password: text})}
                value={this.state.password}
              />
            </View>
          </View>
          <TouchableOpacity style={styles.rowSavePassword}>
            <View style={styles.btnSavePassword}>
              <View style={styles.circleSavePassword} />
            </View>
            <Text>{this.state.arrRange.toString()}</Text>
          </TouchableOpacity>
        </View>
        {/* --- button login --- */}
        <TouchableOpacity
          style={styles.btnLogin}
          onPress={() => {
            this.props.navigation.navigate('Home');
          }}>
          <Text style={styles.txtLogin}>LOGIN</Text>
        </TouchableOpacity>

        {/* ================= DEMO REDUX================ */}

        <View>
          <Text>{this.props.counter}</Text>
          <TouchableOpacity onPress={this.handleIncrease}>
            <Text>+</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.handleDecrease}>
            <Text>-</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: any) => ({
  counter: state.counter,
});

export default connect(mapStateToProps, actions)(index);
