import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  // ========== login form ========== //
  formLogin: {
    width: width * 0.9,
    backgroundColor: 'rgba(0,100,255,0.3)',
    borderRadius: 5,
  },
  formInput: {
    marginHorizontal: 10,
  },
  rowInput: {
    flexDirection: 'row',
    paddingVertical: 5,
    marginTop: 10,
  },
  txtLeftRowInput: {
    width: '40%',
  },
  txtRightRowInput: {
    width: '60%',
    color: 'red',
  },
  rowSavePassword: {
    flexDirection: 'row',
    paddingVertical: 5,
    marginTop: 10,
    justifyContent: 'center',
  },
  btnSavePassword: {
    height: 16,
    width: 16,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    padding: 2,
    marginRight: 10,
  },
  circleSavePassword: {
    borderRadius: 2,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
  // ========== button login ========== //
  btnLogin: {
    backgroundColor: 'green',
    width: width * 0.5,
    paddingVertical: 15,
    borderRadius: 15,
    marginTop: 50,
    alignItems: 'center',
  },
  txtLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default styles;
