import {INCREASE, DECREASE} from './Action-type';

export const counterIncrease = () => ({type: INCREASE});
export const counterDecrease = () => ({type: DECREASE});
