import {combineReducers} from 'redux';
import counterReducer from './Reducer';

export default combineReducers({
  counter: counterReducer,
});
