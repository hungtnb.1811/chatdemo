import {firestore} from 'react-native-firebase';

export async function onLoadMessage(param: {roomPath: string}) {
  let mesPath = await firestore().collection('listMessage');
  let done = mesPath
    .get()
    .then((item: any) =>
      item._docs.map((e: any) => e._data.ID_room == param.roomPath && e._data),
    )
    .catch(err => console.log(err));
  return done;
}
