import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {},
  // ===== Information of group chat ===== {member, numOfMember, big avatar, description}
  infoGroupChat: {
    width: '100%',
    alignItems: 'center',
  },
  avatar: {
    width: width * 0.4,
    height: width * 0.4,
    borderRadius: width * 0.8,
  },
  boxInfo: {
    alignItems: 'center',
    marginTop: 10,
  },
  txtInfo: {
    color: 'gray',
    fontWeight: '600',
    marginBottom: 5,
  },
  // ===== content view ===== {mes,avatar}
  // ----- cover content -----
  contentView: {
    flex: 1,
    marginBottom: 5,
  },
  content: {
    padding: 10,
    height: 300,
  },
  // ----- one row message -----
  rowMessage: {
    width: '100%',
    marginBottom: 15,
  },
  iconAvatarRowMessage: {
    paddingHorizontal: 5,
    alignSelf: 'center',
    fontSize: 16,
  },
  detailRowSentMessage: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
  txtMessage: {
    maxWidth: '70%',
    backgroundColor: 'rgba(0,110,255,0.7)',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
    overflow: 'hidden',
    color: 'white',
  },
  txtMessageSent: {
    textAlign: 'right',
  },
  detailRowRecivedMessage: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
  },
  txtMessageRecived: {
    textAlign: 'left',
  },
  // ===== group handle =====
  // ----- group handle top -----
  handleTop: {
    height: 60,
    width: '100%',
    backgroundColor: 'lightgray',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginBottom: 5,
  },
  oneFeature: {
    width: 50,
    height: 50,
    backgroundColor: 'white',
    marginRight: 7,
    borderRadius: 5,
  },
  // ----- group handle bottom -----
  handleBottom: {
    height: 50,
    width: '100%',
    backgroundColor: 'lightgray',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginBottom: 5,
  },
  leftInputMessagee: {
    flexDirection: 'row',
  },
  optionInput: {
    backgroundColor: 'white',
    width: 35,
    height: 35,
    marginRight: 7,
    borderRadius: 5,
  },
  rightInputMessagee: {
    flex: 1,
    height: 35,
    flexDirection: 'row',
    marginVertical: 7,
    borderRadius: 15,
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  txtInputMessage: {
    paddingHorizontal: 8,
    flex: 1,
    height: '100%',
    textAlign: 'right',
    color: 'gray',
  },
  sendIcon: {
    fontSize: 18,
    paddingLeft: 8,
    paddingRight: 15,
    color: 'rgba(0,110,255,0.7)',
    // marginHorizontal: 5,
  },
});
export default styles;
