import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

interface Props {
  source: Message[];
  style?: any;
  width: any;
  height: any;
  userID: string;
  moreFeature?: any;
  onSendMessage: (mes: string) => void;
  onPullToLoadMore: () => void;
  onListenNewMessage?: () => void;
}

interface Message {
  user: string;
  mes: string;
}

export default class ChatView extends Component<Props> {
  state = {
    message: '',
  };

  _renderRowMessage = (mes: any, index: number) => {
    return mes.user === this.props.userID ? (
      <View key={index.toString()} style={styles.rowMessage}>
        <View style={styles.detailRowSentMessage}>
          <Text style={[styles.txtMessage, styles.txtMessageSent]}>
            {mes.mes}
          </Text>
          <FontAwesome name="user-circle" style={styles.iconAvatarRowMessage} />
        </View>
      </View>
    ) : (
      <View key={index.toString()} style={styles.rowMessage}>
        <View style={styles.detailRowRecivedMessage}>
          <FontAwesome name="user-circle" style={styles.iconAvatarRowMessage} />
          <Text style={[styles.txtMessage, styles.txtMessageSent]}>
            {mes.mes}
          </Text>
        </View>
      </View>
    );
  };

  _showInfoGroup = () => {
    return (
      <View style={styles.infoGroupChat}>
        <View>
          <Image
            style={styles.avatar}
            source={{
              uri:
                'https://baomuctim.com/wp-content/uploads/2018/11/gg-la-gi-0.jpg',
            }}
          />
        </View>
        <View style={styles.boxInfo}>
          <Text style={styles.txtInfo}>Học viên tại BTTOP</Text>
          <Text style={styles.txtInfo}>Hội viên vàng</Text>
        </View>
      </View>
    );
  };

  render() {
    console.log(this.props.source);
    return (
      // --- View container ---
      <View
        style={[
          styles.container,
          {width: this.props.width, height: this.props.height},
          this.props.style,
        ]}>
        {/* --- View content in chat view --- */}
        <View style={styles.contentView}>
          <ScrollView
            ref="_srollViewMessage"
            style={styles.content}
            onScrollEndDrag={event => {
              event.nativeEvent.contentOffset.y <= 0 &&
                this.props.onPullToLoadMore();
            }}>
            {this.props.source.length <= 0 && this._showInfoGroup()}
            {this.props.source.map((mes: any, index: number) =>
              this._renderRowMessage(mes, index),
            )}
          </ScrollView>
        </View>

        {/* --- View handle {input mess, icon, etc} --- */}
        {!this.props.moreFeature && (
          // ----- group handle top
          <View style={styles.handleTop}>
            <ScrollView horizontal={true} style={{flex: 1}}>
              <View style={styles.oneFeature} />
              <View style={styles.oneFeature} />
              <View style={styles.oneFeature} />
              <View style={styles.oneFeature} />
              <View style={styles.oneFeature} />
            </ScrollView>
          </View>
        )}
        {/* ----- group handle bottom */}
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          style={styles.handleBottom}>
          <View style={styles.leftInputMessagee}>
            <View style={styles.optionInput} />
            <View style={styles.optionInput} />
            <View style={styles.optionInput} />
          </View>
          <View style={styles.rightInputMessagee}>
            <TextInput
              onChangeText={mes => this.setState({message: mes})}
              value={this.state.message}
              placeholder="...message"
              // placeholderTextColor="rgba(0,110,255,0.7)"
              style={styles.txtInputMessage}></TextInput>
            <TouchableOpacity
              onPress={async () => {
                this.state.message != '' &&
                  (this.props.onSendMessage(this.state.message),
                  this.setState({message: ''}));

                // @ts-ignore
                this.refs._srollViewMessage.scrollToEnd({animated: true});
              }}>
              <FontAwesome name="send" style={styles.sendIcon} />
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
