import React, {Component} from 'react';
import {Text, View} from 'react-native';
import RootStack from './chatDemo/navigation/RootStack';
import firebase from 'react-native-firebase';
import {Provider} from 'react-redux';
import store from './chatDemo/services/redux/checkLogin/Store';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    );
  }
}
